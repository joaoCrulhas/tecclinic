<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
//Rotas das despesas
Route::get('/despesas','DespesaController@index')->name('despesas.getID');
Route::get('/despesas/{despesa}', 'DespesaController@show')->name('despesas.getID');
Route::post('/despesas', 'DespesaController@store')->name('despesas.getID');
Route::put('/despesas/{despesa}', 'DespesaController@update')->name('despesas.getID');
Route::delete('/despesas/{despesa}', 'DespesaController@delete')->name('despesas.getID');

//Rotas das receitas
Route::get('/receitas','ReceitaController@index')->name('receitas.get');
Route::get('/receitas/{receita}', 'ReceitaController@show')->name('receitas.getID');
Route::post('/receitas', 'ReceitaController@store')->name('receitas.post');
Route::put('/receitas/{receita}', 'ReceitaController@update')->name('receitas.put');
Route::delete('/receitas/{receita}', 'ReceitaController@delete')->name('receitas.delete');

//Rotas dos pacientes
Route::get('/pacientes','PacienteController@index')->name('pacientes.get');
Route::get('/pacientes/{paciente}', 'PacienteController@show')->name('pacientes.getID');
Route::post('/pacientes', 'PacienteController@store')->name('pacientes.create');
Route::put('/pacientes/{paciente}', 'PacienteController@update')->name('pacientes.put');
Route::delete('/pacientes/{paciente}', 'PacienteController@delete')->name('pacientes.delete');;

