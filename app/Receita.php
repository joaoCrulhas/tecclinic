<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Receita extends Model
{
    //
    protected $fillable = ['nome','data_recebimento','valor'];
}
