<?php
namespace App\Http\Controllers;

use App\Despesa;
use Illuminate\Http\Request;

class DespesaController extends Controller
{
    //
    public function index()
    {
        return Despesa::all();
    }

    public function show (Despesa $despesa){
        return  $despesa;
    }

    public function store(Request $request)
    {
        $despesa = Despesa::create($request->all());
        return response()->json($despesa, 201);
    }

    public function update (Request $request, Despesa $despesa){
        $despesa->update($request->all());
        return response()->json($despesa, 200);
    }

    public function delete(Despesa $despesa){
        $despesa->delete();
        return response()->json(null,204);
    }


}
