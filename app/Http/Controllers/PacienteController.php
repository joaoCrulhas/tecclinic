<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Paciente;

class PacienteController extends Controller
{
    public function index()
    {
        return Paciente::all();
    }

    public function show (Paciente $paciente){
        return  $paciente;
    }

    public function store(Request $request)
    {
        $paciente = Paciente::create($request->all());
        return response()->json($paciente, 201);
    }

    public function update (Request $request, Paciente $paciente){
        $paciente->update($request->all());
        return response()->json($paciente, 200);
    }

    public function delete(Paciente $paciente){
        $paciente->delete();
        return response()->json(null,204);
    }

}
