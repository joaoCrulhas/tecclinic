<?php
namespace App\Http\Controllers;

use App\Receita;
use Illuminate\Http\Request;

class ReceitaController extends Controller
{
    //
    public function index()
    {
        return Receita::all();
    }

    public function show (Receita $receita){
        return  $receita;
    }

    public function store(Request $request)
    {
        $receita = Receita::create($request->all());
        return response()->json($receita, 201);
    }

    public function update (Request $request, Receita $receita){
        $receita->update($request->all());
        return response()->json($receita, 200);
    }

    public function delete(Receita $receita){
        $receita->delete();
        return response()->json(null,204);
    }


}
