<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Despesa extends Model
{
    //
    protected $fillable = ['nome','data_vencimento','valor'];

}

