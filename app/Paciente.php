<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Paciente extends Model
{
    protected $fillable =
    ['nome', 'data_nascimento', 'telefone_celular', 'telefone_fixo', 'email',
    'cidade', 'estado', 'rua', 'cep', 'cpf', 'estado_civil', 'rg'];

}
