<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePacientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * Nome : String
     * Data Nascimento : Date
     * Telefone Celular : String
     * Telefone Fixo : String
     * Email : String
     * Rua : String
     * Bairro : String
     * CEP : String Estado : String
     * Cidade : String
     * @return void
     */
    public function up()
    {
        Schema::create('pacientes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome');
            $table->date('data_nascimento');
            $table->string('telefone_celular');
            $table->string('telefone_fixo');
            $table->string('email');
            $table->string('cidade');
            $table->string('estado');
            $table->string('rua');
            $table->string('cep');
            $table->timestamps();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pacientes');
    }
}
