<?php

use Illuminate\Database\Seeder;
use App\Despesa;

class DespesasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Despesa::truncate();
        $faker = \Faker\Factory::create();

        for ($i = 0; $i < 50; $i++) {
            Despesa::create([
                'nome' => $faker->name ,
                'data_vencimento' =>$faker->date ,
                'valor' => $faker->randomNumber(2)
            ]);
        }

    }
}
