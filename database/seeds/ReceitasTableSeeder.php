<?php
use Illuminate\Database\Seeder;
use App\Receita;

class ReceitasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Receita::truncate();
        $faker = \Faker\Factory::create();

        for ($i = 0; $i < 50; $i++) {
            Receita::create([
                'nome' => 'Receita numero ' . $i ,
                'data_recebimento' =>$faker->date ,
                'valor' => $faker->randomNumber(2)
            ]);
        }

    }
}
