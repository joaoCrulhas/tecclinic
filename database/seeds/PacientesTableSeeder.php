<?php

use Illuminate\Database\Seeder;
use App\Paciente;

class PacientesTableSeeder extends Seeder
{

    public function run()
    {
        Paciente::truncate();
        $faker = \Faker\Factory::create();

        for ($i=0; $i < 20 ; $i++) {
            Paciente::create([
                'nome' => $faker->name ,
                'data_nascimento' => $faker->date ,
                'telefone_celular' => $faker->phoneNumber,
                'telefone_fixo' => $faker->phoneNumber,
                'email' => $faker->email,
                'cidade' => $faker->city,
                'estado' => $faker->state,
                'rua' => $faker->streetAddress,
                'cep' => $faker->postcode,
            ]);
        }
    }
}
